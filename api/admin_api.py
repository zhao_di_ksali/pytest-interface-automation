#作者：道长
import  sys
from api import client
import os
import pytest
sys.path.append("..")
#对于模块和自己写的程序不在同一个目录下，可以把模块的路径通过sys.path.append(路径)添加到程序中。
# 在程序开头加上：
# import sys
# sys.path.append(’引用模块的地址')
from common.read_yaml import ReadYaml, ReadYaml_anjiekou

data = ReadYaml("data.yml").get_yaml_data()#读取数据
course_data = ReadYaml_anjiekou("duo_data/admin_data/2,course_data.yml").get_yaml_data()#读取数据
login_data = ReadYaml_anjiekou("duo_data/admin_data/1,login_data.yml").get_yaml_data()#读取数据


class Admin(client.HttpClient):

    def __init__(self):
        super().__init__()
        self.host = data['admin_host']

    """-------------------------------iShow-admin 开始--------------------------------------------------"""
    "总后台-登录"
    data = login_data['login_admin']['data']
    def admin_login0(self,account=data['account'],password=data['password']):
        body = {
                "account": account,
                "password": password
                }
        data = login_data['login_admin']
        url = self.get_full_url(data['url'])
        print("url",url)
        print("body",body)
        return self.send(url,body,method=data['method'])

    "总后台-新增课程"
    data = course_data['add_course']['data']
    def admin_add_kecheng(self,token,coverKey=data['coverKey']
                          ,mode = data['mode']
                          ,name = data['name']
                          ,teacher = data['teacher']
                          ,subject = data['subject']
                          ,type = data['type']):
        body = {
                "coverKey": coverKey,
                "mode": mode,
                "name": name,
                "subject": subject,
                "teacher": teacher,
                "type": type}
        data = course_data['add_course']
        url = self.get_full_url(data['url'])
        return self.send(url,body,method=data['method'],x_token=token)
    "总后台-上架课程"
    data = course_data['shelve_course']['data']
    def admin_shelve_course(self,token,courseId=data['courseId']):
        body = {
                "courseId": courseId}
        data = course_data['shelve_course']
        url = self.get_full_url(data['url'])
        return self.send(url,body,method=data['method'],x_token=token)

    "总后台-下架课程"
    data = course_data['unshelve_course']['data']
    def admin_unshelve_course(self,token,courseId=data['courseId']):
        body = {
                "courseId": courseId}
        data = course_data['unshelve_course']
        url = self.get_full_url(data['url'])
        return self.send(url,body,method=data['method'],x_token=token)

    """-------------------------------iShow-admin 结尾--------------------------------------------------"""

    # 下面是etc和replace用法举例，不过没进行yaml数据替换，不过都一样
    def admin_get_draw_order(self,ader_id,status,session):
        etc = {
            "start":0,
            "limit":10,
            "otc_ader_id":ader_id,
            "otc_ader_withdraw_status":status
        }
        url =self.get_full_url(self.http_map['draw_list'],etc= etc)
        return self.send(url,x_token=session)

    def admin_update_draw_status(self,id,otc_ader_id,status,session):
        body = {
            "otc_ader_withdraw_status":status,
            "otc_ader_id":otc_ader_id
        }
        url = self.get_full_url(self.http_map['update_draw_status'],replace={id})
        return self.send(url,body,method="patch",x_token=session)
    #
    # def admin_get_merchant_info(self,login_name,session):
    #     etc = {
    #         "start":0,
    #         "limit":999,
    #         "merchant_loginname":login_name
    #     }
    #     url = self.get_full_url(self.http_map['get_merchant_info'],etc=etc)
    #     return self.send(url,x_token=session)
    #
    # def admin_get_transac_list(self,session,order_number = ""):
    #     etc = {
    #         "order_number":order_number
    #     }
    #     url = self.get_full_url(self.http_map['get_recharge_transac_list'],etc=etc)
    #     return self.send(url,x_token=session)
    #
    # def admin_get_withdraw_transac_list(self,session,order_number = ""):
    #     etc = {
    #         "order_number": order_number
    #     }
    #     url = self.get_full_url(self.http_map['get_withdraw_transac_list'], etc=etc)
    #     return self.send(url, x_token=session)
    #
    #
    #
    # def admin_duanxin_yzm(self,mobile,session):
    #     etc = {
    #             "nojson": "true",
    #             "mobile": mobile,
    #             "page": 1
    #             }
    #     url = self.get_full_url(self.http_map['admin_duanxin_yzm'], etc=etc)
    #     return self.send(url, x_token=session)
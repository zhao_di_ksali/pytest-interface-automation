"""同action_admin"""

import allure
import sys

sys.path.append("..")

from api import  app_api


class App():
    def __init__(self,app_info):
        self.username = app_info['user_name']
        self.password = app_info['password']
        self.app_api_client = app_api.App()
        result = self.app_login()
        print("result",result)
        assert result['status_code'] == 200,"总后台管理员登录失败"
        self.token = result['data']['data']['token']
        # self.session = result['data']['token']
    """-------------------------------iShow-admin--------------------------------------------------"""

    @allure.step("App账号登录")
    def app_login(self):
        m = app_api.App()
        return m.app_login0(self.username,self.password)

    @allure.step("App账号登录2")
    def app_login1(self,**kwargs):
        m = app_api.App()
        return m.app_login0(**kwargs)

    @allure.step("App新增课程")
    def add_kecheng(self,**kwargs):
        return self.app_api_client.app_add_kecheng(self.token,**kwargs)
    """-------------------------------iShow-admin--------------------------------------------------"""
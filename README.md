#作者：道长
**框架介绍**

Python+pytest+allure+log+yaml（默认数据和动态传参）+
数据驱动+关键字驱动+单接口参数化+多接口动态传参+pymysql
（数据清理尽量接口清理）+一键切换环境+多角色随意切换
（admin后台，SaaS后台，APP卖家，APP买家）+Jenkins持
续集成+Git+钉钉输出测试报告+并发执行+emoji表情+mark等操作+
每个测试用例独立

目录介绍
action:关键字驱动，所有接口从这里调
allure-report:存放allure测试报告
allure-results:存放数据
api:存放接口和请求封装，之后传给action
case:存放测试用例
common:存放公共部分yaml操作以及数据库等
configs:存放域名，登录，数据库等数据
data:存放yaml测试用例数据
log:存放日志
main：主函数
pytest.ini:配置文件

使用说明后续补充
加我微信：hz223336,框架不断优化更新
加我进微信群：测试之道，欢迎你的加入
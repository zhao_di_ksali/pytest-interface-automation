#作者：道长
from api import app_api
import pytest
import  sys
sys.path.append("..")
from common.db import *
from action import action_admin,action_saas
from common.read_yaml import ReadYaml
data = ReadYaml("data.yml").get_yaml_data()#读取数据

# -----------------------------进行总后台登录--------------------------------------------
@pytest.fixture(scope="session")
def admin_action(admin_info):
    return action_admin.Admin(admin_info)

# -----------------------------进行SaaS后台登录--------------------------------------------
@pytest.fixture(scope="session")
def saas_action(saas_info):
    return action_saas.Saas(saas_info)

# -----------------------------第一个人进行app登录--------------------------------------------
@pytest.fixture(scope="session")
def first_action(first_info):
    return  action_saas.Saas(first_info)

# # -----------------------------第二个人进行app登录--------------------------------------------
# @pytest.fixture(scope="session")
# def second_ad_action(second_ad_info):
#     return  action_saas.Saas(second_ad_info)
#
# # -----------------------------第三个人进行app登录(hbq)--------------------------------------------
# @pytest.fixture(scope="session")
# def three_ad_action(three_ad_info):
#     return app_api.Otc(three_ad_info)


# -----------------------------获取账号otc_admin数据-------------------------------------
@pytest.fixture(scope="session")
def admin_info():
    return data['admin']
# -----------------------------获取账号企业平台merchant数据------------------------------
@pytest.fixture(scope="session")
def saas_info():
    return data['saas']
# -----------------------------获取账号user数据-------------------------------------
@pytest.fixture(scope="session")
def first_info():
    return  data['user']



# # -----------------------------获取账号user_second数据------------------------------
# @pytest.fixture(scope="session")
# def second_info():
#     return  data['user_second']
# # -----------------------------获取账号user_three数据------------------------------
# @pytest.fixture(scope="session")
# def three_info():
#     return  data['user_three']





# # -----------------------------------第一，第二,第三个人登录的session---------------------------
# @pytest.fixture(scope="session")
# def first_session(first_action):
#     return first_action.session
# @pytest.fixture(scope="session")
# def second_session(second_action):
#     return second_action.session
# @pytest.fixture(scope="session")
# def three_session(three_action):
#     return three_action.session
#
# @pytest.fixture(scope="session")
# def admin_three_session(admin_action):
#     return admin_action.session